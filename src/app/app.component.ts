import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Frontend';

  login: boolean;
  isHome: boolean;
  isLogged: boolean;
  userLogged: any;

  constructor(private router: Router,private userService: UserService){
    this.login = false;
    this.isHome = false;

    this.router.events.subscribe((url:any) => {
      if( url instanceof NavigationStart) {
        console.log(url);
        if( url.url == "/login" || url.url == "/" || url.url == "/#/" ){
          this.isHome = true;
          this.isLogged = false;
        }else{
          this.isHome = false;
          this.userLogged = this.userService.getUserLoggedIn();
          if( this.userLogged != null && this.userLogged !== undefined || this.userLogged !== ""){
            this.isLogged = true;
          }
        }
      }
    });

  }

}
