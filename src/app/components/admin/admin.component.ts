import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { Producto } from 'src/app/model/producto.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit  {

  constructor(private  prodService: ProductosService) { }

  @ViewChild('myForm', {static: false}) myForm: ElementRef;

  editField: string;
  prodList: Array<any> = [];

  awaitingprodList: Array<any> = [];

  ngOnInit(): void {
    this.prodService.getProductos().subscribe( (res: any) => {
      this.prodList = res.data;
    });
  }

  updateList(id: number, property: string, prodt: Producto, event: any) {
    this.editField = event.target.textContent;
    prodt[property] = this.editField;
    console.log(prodt.id);
    this.prodService.saveProducto(prodt).subscribe( (res: any) => {
    });
  }

  remove(id: any, prodt: Producto) {
    prodt['estado'] = 0;
    this.prodService.saveProducto(prodt).subscribe( (res: any) => {
      this.prodList.splice(id, 1);
    });
  }

  add(nombre: any ,
      descripcion: any ,
      precioCompra: any ,
      precioVenta: any ,
      tipoProducto: any ,
      cantidad: any ,
      alto: any ,
      ancho: any ,
      largo: any ,
      estado: any ) {

      let prodt = new Producto();
      prodt.nombre = nombre;
      prodt.descripcion = descripcion;
      prodt.precioVenta = precioVenta;
      prodt.precioCompra = precioCompra;
      prodt.tipoProducto = tipoProducto;
      prodt.cantidad = cantidad;
      prodt.alto = alto;
      prodt.ancho = ancho;
      prodt.largo = largo;
      prodt.estado = estado;
      // = [nombre,precioVenta,cantidad,tipoProducto,descripcion];
      this.prodService.saveProducto(prodt).subscribe( (res:any) =>{
        this.prodList.push(res.data);
        this.myForm.nativeElement.reset();
      });
    /*
    if (this.awaitingprodList.length > 0) {
      const person = this.awaitingprodList[0];
      this.prodList.push(person);
      this.awaitingprodList.splice(0, 1);
    }
    */
  }

  changeValue(id: any, property: string, prodt: Producto, event: any) {
    //this.editField = event.target.textContent;
    //prodt[property] = this.editField;
    //console.log(prodt);
  }



}
