import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  currentUser: string;

  constructor(private router: Router,private userService: UserService, private elementRef: ElementRef) {
    this.currentUser = this.userService.getUserLoggedIn();
    if( this.currentUser == null || this.currentUser == undefined || this.currentUser == ""){
      this.navigate();
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundImage = '';
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'whitesmoke';
 }

  navigate() {
    this.router.navigateByUrl('/login');
  }

}
