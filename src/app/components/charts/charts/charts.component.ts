import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../../assets/canvasjs.min';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

	arrayChart: Array<ChartGraph> = [];

  constructor(private  prodService: ProductosService) { }

  ngOnInit() {

    this.prodService.getProductos().subscribe( (res: any) => {
		for(let i = 0; i < res.data.length; i++){
			let chartPrd = new ChartGraph();
			chartPrd.y = res.data[i].cantidad;
			chartPrd.label = res.data[i].nombre+" ("+res.data[i].cantidad+")";
			this.arrayChart.push(chartPrd);
		}

		
		console.log(this.arrayChart);

		let chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
				text: "Stock Productos"
			},
			data: [{
				type: "column",
				dataPoints: this.arrayChart
			}]
		});
		
		chart.render();
	});


		
  }


}

  class ChartGraph{
	y: any;
	label: any;
  }