import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { LoginService } from 'src/app/services/login-service.service';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  implements OnInit, AfterViewInit  {

  currentUser: string;

  constructor(private loginService: LoginService, private router: Router,private userService: UserService, private elementRef: ElementRef) {
    this.currentUser = this.userService.getUserLoggedIn();
    if( this.currentUser != null && this.currentUser != undefined && this.currentUser != ""){
      this.navigate();
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundImage = 'url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg")';
 }
  logIn(username: string, password: string, event: Event) {
    event.preventDefault(); // Avoid default action for the submit button of the login form
    // Calls service to login user to the api rest

    this.loginService.login(username, password).subscribe(

      (res:any) => {
        if(res.result){
          this.userService.setUserLoggedIn(res.data);
          this.navigate();
        }
      },
      error => {
        console.error(error);

      }
    );
  }

  navigate() {
    this.router.navigateByUrl('/dashboard');
  }

}
