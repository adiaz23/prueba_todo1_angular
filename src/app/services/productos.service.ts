import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Producto } from '../model/producto.model';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http: HttpClient) { }

  
  getProductos() {
    return this.http.get(`http://localhost:8080/getProductos`);
  }

  saveProducto(producto: Producto){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })};
      /*
    const formData = new FormData();
    formData.append('nombre', producto.nombre);
    formData.append('precioVenta', producto.precioVenta);
    formData.append('precioCompra', producto.precioCompra);
    formData.append('descripcion', producto.descripcion);
    formData.append('tipoProducto', producto.tipoProducto);
    formData.append('alto', producto.alto);
    formData.append('ancho', producto.ancho);
    formData.append('largo', producto.largo);
    formData.append('estado', producto.estado);
    */
    const params = new HttpParams({
      fromObject: {
        id: producto.id,
      nombre: producto.nombre ,
      descripcion: producto.descripcion ,
      precioCompra: producto.precioCompra ,
      precioVenta: producto.precioVenta ,
      tipoProducto: producto.tipoProducto ,
      cantidad: producto.cantidad ,
      alto: producto.alto ,
      ancho: producto.ancho ,
      largo: producto.largo ,
      estado: producto.estado
      }
    });
    return this.http.post(`http://localhost:8080/saveProductos`, params, httpOptions);
  }
}
